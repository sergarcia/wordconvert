"""
Currently works with numbered reference

Notes:
    * Currently works with numeric references of the type '[1]'
    * Ussing the .bbl file to get reference information. Note that his file follows the order of the manuscript references.
    * Currently assums that .bbl file is named like docx file. Output file name will also be derived from docx input name

Usage:

    `python ref2endnote 'input.docx'`

"""

from docx import Document
import re
import sys

def main():

    input_doc_path = sys.argv[1]
    input_bbl_path = input_doc_path[:-5] + '.bbl'
    output_doc_path = input_doc_path[:-5]+'-endnoteref.docx'

    doc = Document(input_doc_path)

    ref_dict = parse_references(input_bbl_path)

    for paragraph in doc.paragraphs:
        # Case 1, one citation
        cit_numbers = re.findall('\[(\d)\]',paragraph.text)
        for number in cit_numbers:
            paragraph.text = re.sub('\['+number+'\]', ref_dict[number],paragraph.text)

        # Case 2, multiple citations together
        mcit_numbers = re.findall('\[(.*?)\]', paragraph.text)

        for mcit_number in mcit_numbers:
            cit_str = ''
            for number in mcit_number.split(', '):
                cit_str += ref_dict[number]
            paragraph.text = re.sub('\['+mcit_number+'\]', cit_str, paragraph.text)

    doc.save(output_doc_path)


def parse_references(input_doc_path):
    """
    Parses references into a dictonary

    Args:
        input_doc_path (str): Path to the bbl file corresponding to the document.
    Returns:
        ref_dict (dict): keys are number strings (e.g. '1','2') values are {Smith, 2001}, {Smith, 2001b}, etc.
    """

    ref_number = 0
    ref_dict = {}

    with open(input_doc_path) as f:
        for line in f:
            if line.startswith(r'\bibitem'):
                ref_number += 1
                refmatch = re.search('\{([A-Z,a-z]+)(\d+[a-z]*)\}', line)
                author = refmatch.group(1)
                year = refmatch.group(2)
                ref_dict[str(ref_number)] = '{' + author +', ' + year + '}'

    return ref_dict

main()