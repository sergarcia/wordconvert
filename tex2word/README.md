# ref2endnote
Automatically converts text based references in a MS word document to EndNote linked references.
## Context
This is part of a LaTeX to Word conversion pipeline. Although it may be used for other cases.

If you are unfortunate enough so that you must convert a LaTeX document to Word, 
another requirement may be for the references to be in EndNote. If that is the case this code will help you quickly accomplish so.
## Usage

1. The follwoing files are required:
    * Word document with references in "plain text" `input.docx`.
    * bbl file from LaTeX document `input.bbl`
    * Endnote library `examplelib.enl`

2. Run `python ref2endnote 'input.docx'` with the only argument being the path of your input file.

3. Open Word go to the EndNote tab and click Update Citations and Bibliograpy. 
EndNote uses a tracking number when first authors appear in multiple papers in your library which I currently don't know how to extract.
This will cause it to prompt you to confirm which paper from that author in that year you are trying to cite 
(which will be obvious, since the inserted citation corresponds to the first author). First authors which appear in only one paper will update automatically.

## Contributions
If you would like to add other formatting specifications or IO options to suit your case, please submit a PR!
