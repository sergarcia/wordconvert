Convert Word document with Endnote citations to latex with bibtex references

# Usage instructions
1. Open file in Word and switch Endnote citation style to bibtex export. If hyperlinks are present in your Word document (e.g., citation hyperrefs) they need to be removed by using `ctrl + a ` and then `ctrl + shift + F9`.
2. `pandoc file.docx -o file.tex`
3. Manually extract the reference section from `file.tex` and place it into a new file
   `file.txt`.
4. `mapref.py file` to generate `main.tex` and `main.bib` files corresponding
   to the desired output.
