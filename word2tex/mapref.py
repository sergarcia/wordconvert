#!/usr/bin/env python3

"""
Convert word document with endnote citations to latex with bibtex references. See print_instructions() for additional details.
"""

import argparse
import re

class PrintInstructions(argparse.Action):
    def __call__(self, parser, namespace, values, option_string):
        i = """
        Convert word document with endnote citations to latex with bibtex references.

        Bibliography entries that do not contain author or year, or author name includes non-ASCII characters are saved in a fileand citation key is not updated

        # Usage instructions
        1. Open file in word and switch endnote citation style to bibtex export.
        2. `pandoc file.doc -o file.tex` (pandoc version 2.6)
        3. Manually extract the reference section from `file.tex` and place it into a new file
           `file.txt`.
        4. `mapref.py file` to generate `main.tex` and `main.bib` files corresponding
           to the desired output. The resulting `main.tex` likely needs to be manually adjusted to reflect section, subsections, etc., since Pandoc does not understand wordlet idiosyncrasies (e.g., use boldface for section headers instead of inputing them as section headers, use cursive boldface for subsections instead of inputing them as subsections, etc.).
           """
        print(i)
        parser.exit()


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('file_id', help='Name of the converetd file (see usage instructions), without file extension')
    parser.add_argument('-p', '--paragraphdetection', choices=[0,1], help='Will alter the behavior of paragraph detection but the choice might depend on document type', default=0, type=int)
    parser.add_argument('--print_instructions', action=PrintInstructions, help='Prints instructions associated with the use of this script and exits', nargs=0)
    args = parser.parse_args()

    file_paths = get_file_paths(args)

    old2new = parse_references(file_paths)

    # Read main text
    with open(file_paths['main_in'], 'r') as f:
        lines = [line for line in f]

    lines = reformat_newlines(lines, args)

    lines = reformat_sections(lines)

    lines = update_citations(lines, old2new)

    # Write final tex file
    with open(file_paths['main_out'], 'w') as f:
        [f.write(line) for line in lines]
    print("Wrote: {}".format(file_paths['main_out']))


def get_file_paths(args):
    file_paths = {}
    file_paths['ref_file'] = '{}.txt'.format(args.file_id)
    file_paths['ref_out'] = '{}.bib'.format(args.file_id)
    file_paths['main_in'] =  '{}.tex'.format(args.file_id)
    file_paths['main_out'] = '{}_map.tex'.format(args.file_id)
    file_paths['unmatched_refs'] = '{}_missing_refs.bib'.format(args.file_id)
    return file_paths


def parse_references(file_paths):
    #1. Read bib file and parse entries to list
    with open(file_paths['ref_file'],'r') as f:
        entries = f.read().replace('\n', ' ').replace('\r', '').replace('\{', '{').replace('\}', '}').replace('@', '\n@').split('\n')
    if entries[0] != '@': #Sometimes the first element may contain junk
        entries.pop(0)

    # Update citekeys to authorYear, crete mapping dict, avoid repeated keys.
    new2old = {}
    old2new = {}
    new_entries = []
    unmatched_refs = []
    for entry in entries:
        citekey = re.search('@[a-z]+{([A-Z]+[0-9]+),.*',entry).group(1)
        try:
            author = re.search('.*author = {([A-Z]*[A-Za-z\-]+)(,| ).*',entry).group(1)
            year = re.search('.*year = {([0-9]+).*',entry).group(1)
            new_key = author.lower() + year
            if new_key in new2old:
                new_key = new_key + 'b'
            if new_key in new2old:
                new_key = new_key + 'c'
        except Exception as e:
            new_key = citekey
            unmatched_refs.append(entry)

        new2old[new_key] = citekey
        old2new[citekey] = new_key
        new_entries.append(re.sub('RN[0-9]+', new_key, entry))

    # Write out new ref file, proper format is @article{citekey,\n title={},\n ...\n}\n@article...
    with open(file_paths['ref_out'], 'w') as f:
        for entry in new_entries:
            f.write(entry.replace('},','},\n ').replace(', author', ',\n  author')[:-1] + '\n}\n')
    print("Wrote: {}".format(file_paths['ref_out']))

    # Write out unmatched refs
    if unmatched_refs:
        with open(file_paths['unmatched_refs'], 'w') as f:
            [f.write(ref) for ref in unmatched_refs]
        print("Wrote: {}".format(file_paths['unmatched_refs']))
    else:
        print("All references matched")

    return old2new


def reformat_newlines(lines, args):
    """
    A simple approach is this: however it looses paragraph structure
    # Remove all newlines and add newline after period
    return [line.replace('\n\n','$$$$').replace('\n', ' ').replace('. ', '.\n').replace('%', '\n')
            for line in lines]
    """
    # Join text until period or empty line then insert newline
    # if preceded by empty and does not start with \, it is begining of paragraph, until next empty line.
    # For paragraph remove all new lines and add newlines after .
    p_start_toggle = True # WIP this might be added as an option if it makes sense
    lines2 = []
    last_line = 'foo'
    in_paragraph = False
    def test_pstart(last_line, line):
        if args.paragraphdetection == 0:
            return not last_line.strip()
        elif args.paragraphdetection == 1:
            return not last_line.strip() and line[0] != '\\'

    for line in lines:
        # Detect start of paragraph
        if test_pstart(last_line, line):
            in_paragraph = True
            paragraph = line

        # Detect end of paragraph
        elif in_paragraph and (not line.strip()):
            lines2.append(paragraph.replace('\n', ' ') + '\n\n') # note the space after . is good to avoid false positives, for example, e.g.
            in_paragraph = False

        elif in_paragraph:
            paragraph += line

        else:
            lines2.append(line)

        last_line = line
    return lines2


def reformat_sections(lines):
    # Convert \textbf{\emph{(.*)}} or \emph{\textbf{(.*)}} into \subsection{(.*)}\n
    # Does not work if it spawns two lines
    # Section: \\textbf{(.*)}\n
    # This may not be needed with all documents
    lines0 = []
    for line in lines:
        match1 = re.findall(r'\\emph\{\\textbf\{(.*)(?:\.)\}(?:\.)\}', line)
        match2 = re.findall(r'\\textbf\{\\emph\{(.*)(?:\.)\}(?:\.)\}', line)
        if match1 or match2:
            if match1:
                section_title = match1[0]
            elif match2:
                section_title = match2[0]
            lines0.append(re.sub(r'(\\emph\{\\textbf\{.*\}\})|(\\textbf\{\\emph\{.*\}\})',
                r'\\subsection{' + section_title + '}\n', line))
            lines0.append("") #insert empty line after subsection
        else:
            lines0.append(line)
    return lines0


def update_citations(lines, old2new):
    # Convert \textbackslash{}cite\{(.*)\} into \supercite{(new)}
    # Note: if cite\{} spans multiple lines this will not work. But that should not occur based on the previous manipulation.
    lines_f = []
    for line in lines:
        for match in re.findall(r'\\textbackslash(?:{}| )cite\\{RN[0-9]+(?:(?:, [0-9]+)+)?\\}(?:\n| |.)', line):
            # When hitting an instance of \cite{RNXX} or \cite{RNXX, XX, XX}
            incite = re.search(r'\\textbackslash(?:{}| )cite\\{(.*)\\}', match).group(1)
            cases = incite.split(',')
            cases[0] = cases[0][2:] # Drop RN in the first case
            new_incite = ', '.join([old2new['RN' + case.strip()] for case in cases])
            newcite = r'\\supercite{' + new_incite + r'}'
            line = re.sub(r'\\textbackslash(?:{}| )cite\\{'+incite+r'\\}', newcite,line)
        lines_f.append(line)
    return lines_f


if __name__ == '__main__':
   main()
