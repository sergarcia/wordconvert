These scripts are part of a pipeline to convert between MS Word and tex.
Pandoc or similar tools can be used for the first step, and then the scripts provided here help linking reference information in case the Word file is using EndNote.
